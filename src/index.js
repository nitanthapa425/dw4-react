import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import App1 from "./ComponentPractice/App1";
import MapPractice from "./ComponentPractice/MapPractice";
import LearnUseState1 from "./ComponentPractice/LearnUseState/LearnUseState1";
import LearnUseState2 from "./ComponentPractice/LearnUseState/LearnUseState2";
import LearnUseState3 from "./ComponentPractice/LearnUseState/LearnUseState3";
import LearnUseState4 from "./ComponentPractice/LearnUseState/LearnUseState4";
import LearnUseStateRendering1 from "./ComponentPractice/LearnUseState/LearnUseStateRendering1";
import HideAndShowToggle from "./ComponentPractice/LearnUseState/HideAndShowToggle";
import PreventInfiniteLoop from "./ComponentPractice/LearnUseState/PreventInfiniteLoop";
import AsyncBehaviourOfUseState from "./ComponentPractice/LearnUseState/AsyncBehaviourOfUseState";
import AsyncBehaviourOfUseState2 from "./ComponentPractice/LearnUseState/AsyncBehaviourOfUseState2";
import Main from "./ComponentPractice/LearnUseState/Propsdrilling/Main";
import LearnUseEffect1 from "./ComponentPractice/LearnUseEffect/LearnUseEffect1";
import LearnUseEffect2 from "./ComponentPractice/LearnUseEffect/LearnUseEffect2";
import AppApp from "./AppApp";
import { BrowserRouter } from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <div>
      {/* <React.StrictMode> */}
      {/* <App name="nitan" address="gagalphedi" age={29}></App> */}
      {/* <App1></App1> */}
      {/* <MapPractice></MapPractice> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <LearnUseState2></LearnUseState2> */}
      {/* <LearnUseState3></LearnUseState3> */}
      {/* <LearnUseState4></LearnUseState4> */}
      {/* <LearnUseStateRendering1></LearnUseStateRendering1> */}
      {/* <HideAndShowToggle></HideAndShowToggle> */}
      {/* <PreventInfiniteLoop></PreventInfiniteLoop> */}
      {/* <AsyncBehaviourOfUseState></AsyncBehaviourOfUseState> */}
      {/* <AsyncBehaviourOfUseState2></AsyncBehaviourOfUseState2> */}
      {/* <Main></Main> */}
      {/* <LearnUseEffect1></LearnUseEffect1> */}
      {/* <LearnUseEffect2></LearnUseEffect2> */}

      {/* <App></App> */}
      <AppApp></AppApp>
    </div>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
