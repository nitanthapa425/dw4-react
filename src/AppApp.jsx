import React from "react";
import { NavLink, Route, Routes } from "react-router-dom";

import CreateProduct from "./ComponentPractice/LearnToHitApi/Product/CreateProduct";
import GetAllProduct from "./ComponentPractice/LearnToHitApi/Product/getAllProduct";
import EditForm from "./ComponentPractice/LearnToHitApi/Product/EditProduct";
import DetailProducts from "./ComponentPractice/LearnToHitApi/Product/DetailProducts";
import NavLinks from "./ComponentPractice/NavLinks";
import MyRoutes from "./ComponentPractice/MyRoutes";

const AppApp = () => {
  return (
    <div>
      <NavLinks></NavLinks>
      <MyRoutes></MyRoutes>

    </div>
  );
};

export default AppApp;
