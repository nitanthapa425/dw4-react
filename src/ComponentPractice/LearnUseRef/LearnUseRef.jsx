import React, { useRef, useState } from "react";

//useState
//useEffect
//useRef
//define ref
// attached ref

const LearnUseRef = () => {
  let [name, setName] = useState();

  let nameRef = useRef(); //define ref

  let ageRef = useRef();

  let sebRef = useRef();

  return (
    <div>
      <div ref={sebRef}>I am miss seb I am beautiful</div>
      {/* when button is click  element => style=> backgounde => red */}

      <label htmlFor="name">Name</label>
      <input
        type="text"
        id="name"
        value={name}
        onChange={(e) => {
          setName(e.target.value);
        }}
        ref={nameRef}
      ></input>

      <br></br>
      <br></br>
      <input ref={ageRef}></input>

      <br></br>
      <br></br>

      <button
        onClick={() => {
          nameRef.current.focus();
        }}
      >
        Focus Input1
      </button>

      <br></br>

      <button
        onClick={() => {
          ageRef.current.focus();
        }}
      >
        Focus Input 2
      </button>

      <button
        onClick={() => {
          sebRef.current.style.backgroundColor = "black";
        }}
      >
        Chage bg of seb
      </button>
    </div>
  );
};

export default LearnUseRef;
