import React from "react";
import { NavLink, useNavigate } from "react-router-dom";

const NavLinks = () => {
  let token = localStorage.getItem("token");

  let navigate = useNavigate();
  return (
    <div>
      <NavLink to="/products/create" style={{ marginLeft: "20px" }}>
        create
      </NavLink>
      <NavLink to="/products" style={{ marginLeft: "20px" }}>
        products
      </NavLink>

      <NavLink
        to="/register"
        style={{
          marginLeft: "20px",
        }}
      >
        Register
      </NavLink>
      {token ? null : (
        <NavLink
          to="/registers/login"
          style={{
            marginLeft: "20px",
          }}
        >
          Login
        </NavLink>
      )}

      <button
        onClick={() => {
          localStorage.removeItem("token");
          navigate("/registers/login");
        }}
      >
        Logout
      </button>

      {token ? (
        <NavLink
          to="/myProfile"
          style={{
            marginLeft: "20px",
          }}
        >
          My Profile
        </NavLink>
      ) : null}
    </div>
  );
};

export default NavLinks;
