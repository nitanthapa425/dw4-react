import React, { useState } from "react";

const LearnToHandleArray = () => {
  let [data, setData] = useState([
    {
      name: "nitan",
      age: 29,
      isMarried: false,
    },
    {
      name: "seer",
      age: 19,
      address: "sankhu",
      isMarried: true,
    },
    {
      name: "sujan",
      age: 27,
      address: "sifal",
      isMarried: true,
    },
  ]);

  return (
    <div>
      {data.map((value, i) => {
        return (
          <div
            key={i}
            style={{ border: "solid red 3px", marginBottom: "10px" }}
          >
            <p>Name : {value.name ? value.name : "N/A"}</p>
            <p>Age : {value.age ? value.age : "N/A"}</p>
            <p>Address : {value.address ? value.address : "N/A"}</p>

            <p>isMarried: {value.isMarried ? "Yes" : "No"}</p>
          </div>
        );
      })}
    </div>
  );
};

export default LearnToHandleArray;
