import React from "react";

const App1 = () => {
  let name = "nitan";
  let age = 29;

  let isMarried = false; //boolean are not shown in browser(normal pannedl)
  let list = ["nitan", "ram"]; //big bracket are not shown
  let obj = {
    address: "gagalphedi",
  };
  //dont use object as react child
  return (
    <div>
      name is {name}
      <br></br>
      age is {age}
      <br></br>
      is he married? {isMarried === true ? "Yes" : "No"}
      <br></br>
      {list}
      {/* {obj} */}
    </div>
  );
};

export default App1;
