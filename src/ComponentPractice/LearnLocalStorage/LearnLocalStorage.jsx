import React from "react";

const LearnLocalStorage = () => {
  let name1 = "nitan";
  let age1 = 29;
  let isMarried = false;

  let ar1 = [1, 2, 3];
  let info = { address: "gagalphedi" };
  localStorage.setItem("name", name1);
  localStorage.setItem("age", age1);
  localStorage.setItem("isMarried", isMarried);

  localStorage.setItem("ar1", JSON.stringify(ar1));
  localStorage.setItem("info", JSON.stringify(info));

  //to get data from localstorage

  console.log(localStorage.getItem("name"));
  console.log(localStorage.getItem("age"));
  console.log(localStorage.getItem("isMarried"));
  console.log(localStorage.getItem("ar1"));
  console.log(localStorage.getItem("info"));

  return <div>LearnLocalStorage</div>;
};

export default LearnLocalStorage;
