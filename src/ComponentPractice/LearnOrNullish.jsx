import React from "react";

const LearnOrNullish = () => {
  //   let a = 0;
  //   let b = false;
  //   let c = a || b || 0;

  //   console.log(c);

  let a;
  let b = false;
  let c = a ?? b ?? null;

  console.log(c);

  return <div>LearnOrNullish</div>;
};

export default LearnOrNullish;
