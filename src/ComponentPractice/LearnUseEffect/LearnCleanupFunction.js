import React, { useEffect } from "react";

// 1)when component is removed  noting execute except  clean up function
// 2) cleanup function is named as cleanup and its uses

//Mount ==> component mount ===  show component
//Unmount  ===>  component unmount   ===>  hide component

const LearnCleanupFunction = () => {
  console.log("hello, i will not execute when component is unmount");
  useEffect(() => {
    console.log(
      "i am useEffect, i will not execute when component is unmount "
    );
    return () => {
      console.log(
        "i am clean up function ,i will execute when component is unmount"
      );
    };
  }, []);

  return <div>LearnCleanupFunction</div>;
};

export default LearnCleanupFunction;
