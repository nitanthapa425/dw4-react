import React, { useEffect, useState } from "react";

//1 render
//2 render
//3 render
// 4 render

//useEffect
// for first render ( useEffect code run)
// but from second render (useEffect code will run on condition , if any of the dependency changes useEffect will run from second render other wise it will run)
// dependency must be variable of the component
// since props is the part of component we can use it as dependency
// don't use imported variable in dependency
// don't use non primitive variable in dependency (always use primitive variable)

const LearnUseEffect1 = () => {
  let [count, setCount] = useState(0); //1

  let [count1, setCount1] = useState(100); //100
  useEffect(() => {
    console.log("i am useEffect");
  }, [count, count1]);
  //   1   ,  100

  return (
    <div>
      count is {count}
      <br></br>
      count1 is {count1}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        increment count
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        increment count1
      </button>
    </div>
  );
};

export default LearnUseEffect1;
