import React, { useEffect, useState } from "react";

// primitive =>page will render if value is change
// non-primitive =>page will render if memory location is changed

const LearnUseEffect2 = () => {
  let [ar1, setAr1] = useState([1, 2]);

  useEffect(() => {
    console.log("i am useEffect");
  }, [JSON.stringify(ar1)]);

  return (
    <div>
      <button
        onClick={() => {
          setAr1([3, 4]);
        }}
      >
        click
      </button>
    </div>
  );
};

export default LearnUseEffect2;
