import React, { useState } from "react";
import MainChild from "./MainChild";

const Main = () => {
  let [name, setName] = useState("nitan");
  return (
    <div>
      GrandParent
      <MainChild name={name}></MainChild>
    </div>
  );
};

export default Main;
