import React, { useState } from "react";

//To Prevent Infinite Loop
//always use setCout or set.... at some event for eg onClick
const PreventInfiniteLoop = () => {
  let [count, setCount] = useState(0); //2
  //   setCount(count + 1);
  return (
    <div>
      {/* {setCount(count + 1)} */}
      count is {count}
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default PreventInfiniteLoop;

// setCount(count + 1) 3
