import React, { useState } from "react";

const LearnUseState3 = () => {
  let [showImage, setShowImage] = useState(true);

  return (
    <div>
      {showImage ? <img src="./favicon.ico" alt="favicon"></img> : null}
      <br></br>
      <button
        onClick={() => {
          setShowImage(true);
        }}
      >
        show Image
      </button>
      <br></br>
      <button
        onClick={() => {
          setShowImage(false);
        }}
      >
        hide Image
      </button>
    </div>
  );
};

export default LearnUseState3;
