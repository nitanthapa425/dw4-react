import React, { useState } from "react";

const AsyncBehaviourOfUseState = () => {
  let [name, setName] = useState("nitan1"); //nitan4
  console.log(name);
  return (
    <div>
      <div>name is {name}</div>
      <button
        onClick={() => {
          setName("nitan2");
          setName("nitan3");
          setName("nitan4");
        }}
      >
        Change name
      </button>
    </div>
  );
};
///name = nitan4

export default AsyncBehaviourOfUseState;

// setName("nitan2");
// setName("nitan3");
// setName("nitan4");

//    name ="nitan4"
