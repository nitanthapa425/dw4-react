import React, { useState } from "react";
import LearnUseState5 from "./LearnUseState5";

const LearnUseState4 = () => {
  let [showComp, setShowComp] = useState(true);

  return (
    <div>
      LearnUseState4
      <div>hello</div>
      <br></br>
      {showComp ? <LearnUseState5></LearnUseState5> : null}
      <button
        onClick={() => {
          setShowComp(true);
        }}
      >
        Show Comp
      </button>
      <br></br>
      <button
        onClick={() => {
          setShowComp(false);
        }}
      >
        Hide Comp
      </button>
    </div>
  );
};

export default LearnUseState4;
