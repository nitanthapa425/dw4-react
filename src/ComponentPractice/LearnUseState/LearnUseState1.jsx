import React, { useState } from "react";

const LearnUseState1 = () => {
  //   let name = "nitan";
  //   name="bindu"

  //define variable in react using useState hook

  let [name, setName] = useState("nitan");
  let [surName, setSurName] = useState("thapa");

  return (
    <div>
      name is {name}
      <br></br>
      sur name is {surName}
      <br></br>
      <button
        onClick={() => {
          setName("bindu");
        }}
      >
        Change name
      </button>
      <button
        onClick={() => {
          setSurName("KC");
        }}
      >
        change sur name
      </button>
    </div>
  );
};

export default LearnUseState1;
