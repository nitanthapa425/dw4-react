import React, { useState } from "react";
// hide and show image
//using single button
const HideAndShowToggle = () => {
  let [showImg, setShowImg] = useState(true);
  return (
    <div>
      {showImg ? <img src="./favicon.ico" alt="favicon"></img> : null}
      <br></br>
      <button
        onClick={() => {
          setShowImg(!showImg);
        }}
      >
        Toggle
      </button>
    </div>
  );
};

export default HideAndShowToggle;
