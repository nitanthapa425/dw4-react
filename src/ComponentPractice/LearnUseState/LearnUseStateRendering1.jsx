import React, { useState } from "react";
//a component will render only  if state variable  change

const LearnUseStateRendering1 = () => {
  let [count1, setCount1] = useState(0);//2
  let [count2, setCount2] = useState(100);//102

  return (
    <div>
      count1 is {count1}
      <br></br>
      count2 is {count2}
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Change count1
      </button>
      <button
        onClick={() => {
          setCount2(count2 + 1);
        }}
      >
        Change count2
      </button>
    </div>
  );
};

export default LearnUseStateRendering1;
