import React, { useState } from "react";

const Form2 = () => {
  let [name, setName] = useState("");
  let [address, setAddress] = useState("");
  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          let data = {
            name: name,
            address: address,
          };
          console.log(data);
        }}
      >
        <div>
          <label htmlFor="name">Name: </label>
          <input
            type="text"
            id="name"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="address">Address: </label>
          <input
            type="text"
            id="address"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          ></input>
        </div>

        <button type="submit">Click me</button>
      </form>
    </div>
  );
};

export default Form2;
