import React, { useState } from "react";

const Form3 = () => {
  let [name, setName] = useState("");
  let [description, setDescription] = useState("");
  let [country, setCountry] = useState("nep"); //"ind"

  //description

  //select
  //radio
  //checkbox

  let countryOptions = [
    { label: "Nepal", value: "nepal" },
    { label: "Finland", value: "finland" },
    { label: "china", value: "china" },
    { label: "india", value: "india" },
  ];

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          let info = {
            name: name,
            description: description,
            country: country,
          };

          console.log(info);
        }}
      >
        <div>
          <label htmlFor="description">Description</label>
          <input
            id="name"
            value={name}
            placeholder="Name"
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="description">Description</label>
          <textarea
            id="description"
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
            placeholder="write your description"
          ></textarea>
        </div>

        {/* <select
          value={country} //""
          onChange={(e) => {
            setCountry(e.target.value);
          }}
        >
          <option value="">Choose Country</option>
          <option value="nep">Nepal</option>
          <option value="fin">Finland</option>
          <option value="ind">India</option>
          <option value="chi">China</option>
        </select>
        <br></br> */}

        <select
          value={country} //"ind"
          onChange={(e) => {
            setCountry(e.target.value); //ind
          }}
        >
          {countryOptions.map((item, i) => {
            return (
              <option key={i} value={item.value}>
                {item.label}
              </option>
            );
          })}
        </select>
        <br></br>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Form3;
