import React, { useState } from "react";

//radio box

//gender
const Form5 = () => {
  let [gender, setGender] = useState("male"); //other

  let handleSubmit = (e) => {
    e.preventDefault();
    let info = {
      gender: gender,
    };
    console.log(info);
  };

  let genderOptions = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
    { label: "a", value: "lll" },
  ];

  return (
    <div>
      <form onSubmit={handleSubmit}>
        {/* radio button */}

        {genderOptions.map((item, i) => {
          return (
            <>
              <label htmlFor={item.value}>{item.label}</label>
              <input
                type="radio"
                id={item.value}
                checked={gender === item.value}
                value={item.value}
                onChange={(e) => {
                  setGender(e.target.value);
                }}
              ></input>
            </>
          );
        })}

        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Form5;
