import React from "react";

const Form1 = () => {
  //text , number,password, email, file ,date, radio , checkbox
  return (
    <>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          console.log("form is submitted");
        }}
      >
        <label htmlFor="name">Name: </label>

        <input
          type="text"
          id="name"
          onChange={(e) => {
            //e.target.value
            console.log(e.target.value);
          }}
        ></input>
        <br></br>
        <label htmlFor="age">Age:</label>
        <input type="number" id="age"></input>
        <br></br>
        <label htmlFor="password">Password:</label>
        <input type="password" id="password"></input>
        <br></br>
        <label htmlFor="email">Personal email:</label>
        <input type="email" id="email"></input>
        <br></br>
        <label htmlFor="date">Dob: </label>
        <input type="date" id="date"></input>
        <br></br>
        <label htmlFor="file">Profile :</label>
        <input type="file" id="file"></input>
        <br></br>
        <input type="checkbox"></input>
        <br></br>
        <input type="radio"></input>
        <br></br>
        <button type="submit">Submit Form</button>
      </form>
    </>
  );
};

export default Form1;
