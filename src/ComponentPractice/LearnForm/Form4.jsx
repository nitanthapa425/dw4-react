import React, { useState } from "react";

//radio box

//gender
const Form4 = () => {
  let [gender, setGender] = useState("male"); //other

  let handleSubmit = (e) => {
    e.preventDefault();
    let info = {
      gender: gender,
    };
    console.log(info);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        {/* radio button */}

        <label htmlFor="male">Male</label>
        <input
          type="radio"
          id="male"
          value="male"
          checked={gender === "male"}
          onChange={(e) => {
            setGender(e.target.value); //male
          }}
        ></input>

        <label htmlFor="female">Female</label>
        <input
          type="radio"
          id="female"
          value="female"
          checked={gender === "female"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        ></input>

        <label htmlFor="other">Other</label>
        <input
          type="radio"
          id="other"
          value="other"
          checked={gender === "other"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        ></input>

        <br></br>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Form4;
