import React from "react";
import { Route, Routes } from "react-router-dom";
import CreateProduct from "./LearnToHitApi/Product/CreateProduct";
import GetAllProduct from "./LearnToHitApi/Product/getAllProduct";
import EditForm from "./LearnToHitApi/Product/EditProduct";
import DetailProducts from "./LearnToHitApi/Product/DetailProducts";
import Register from "./Register";
import Login from "./Login";
import MyProfile from "./MyProfile";

const MyRoutes = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/products/create"
          element={
            <div>
              <CreateProduct></CreateProduct>
            </div>
          }
        ></Route>
        <Route
          path="/products"
          element={
            <div>
              <GetAllProduct></GetAllProduct>
            </div>
          }
        ></Route>

        <Route
          path="/products/edit/:id"
          element={
            <div>
              <EditForm></EditForm>
            </div>
          }
        ></Route>

        <Route
          path="/products/:id"
          element={
            <div>
              <DetailProducts></DetailProducts>
            </div>
          }
        ></Route>

        <Route
          path="/register"
          element={
            <div>
              <Register></Register>
            </div>
          }
        ></Route>

        <Route
          path="/registers/login"
          element={
            <div>
              <Login></Login>
            </div>
          }
        ></Route>
        <Route
          path="/myProfile"
          element={
            <div>
              <MyProfile></MyProfile>
            </div>
          }
        ></Route>
      </Routes>
    </div>
  );
};

export default MyRoutes;
