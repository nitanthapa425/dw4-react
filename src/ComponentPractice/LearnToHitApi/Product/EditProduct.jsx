import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";

// "http://localhost:8000/products/1234213421341234
//   PATCH
//  data

// for detail page
// localhost:8000/products/648936d36a950ede84da1feb
//GET

const EditForm = () => {
  let [name, setName] = useState("");
  let [price, setPrice] = useState("");
  let [isStock, setIsStock] = useState(false);
  let navigate = useNavigate();

  let params = useParams();

  //id = parmas.id

  let getDetails = async () => {
    let results = await axios({
      url: `http://localhost:8000/products/${params.id}`,
      method: "GET",
    });

    setName(results.data.data.name);
    setPrice(results.data.data.price);
    setIsStock(results.data.data.isStock);
  };

  useEffect(() => {
    getDetails();
  }, []);

  let handleSubmit = async (e) => {
    e.preventDefault();
    let info = {
      name: name,
      price: price,
      isStock: isStock,
    };

    //   console.log(data);

    try {
      await axios({
        url: `http://localhost:8000/products/${params.id}`,
        method: "PATCH",
        data: info,
      });

      navigate("/products");
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Name: </label>
          <input
            type="text"
            id="name"
            value={name}
            placeholder="Name"
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>
        <br></br>
        <div>
          <label htmlFor="price">Price: </label>
          <input
            type="number"
            id="price"
            value={price}
            placeholder="Price"
            onChange={(e) => {
              setPrice(e.target.value);
            }}
          ></input>
        </div>
        <br></br>
        <div>
          <label htmlFor="isStock">In Stock: </label>
          <input
            type="checkbox"
            id="isStock"
            checked={isStock === true}
            onChange={(e) => {
              setIsStock(e.target.checked);
            }}
          ></input>
        </div>
        <br></br>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default EditForm;
