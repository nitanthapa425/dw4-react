import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const GetAllProduct = (props) => {
  let [products, setProducts] = useState([]);

  let navigate = useNavigate();

  let getAllData = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/products",
        method: "GET",
      });
      // console.log(result.data.data);
      setProducts(result.data.data);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getAllData();
  }, []);

  return (
    <div>
      {products.map((item, i) => {
        return (
          <div key={i} style={{ border: "solid red 3px", marginBottom: "3px" }}>
            <p>Product Name: {item.name}</p>
            <p>Product Price: {item.price}</p>
            <p>
              Product Availability:{" "}
              {item.isStock ? "Available" : "Not Available"}
            </p>
            <button
              onClick={async () => {
                try {
                  await axios({
                    url: `http://localhost:8000/products/${item._id}`,
                    method: "DELETE",
                  });
                  getAllData();
                } catch (error) {
                  console.log(error.message);
                }
              }}
            >
              Delete
            </button>

            <br></br>
            <button
              onClick={() => {
                navigate(`/products/edit/${item._id}`);
              }}
            >
              Edit
            </button>

            <button
              onClick={() => {
                navigate(`/products/${item._id}`);
              }}
            >
              View
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default GetAllProduct;
