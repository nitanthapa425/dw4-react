import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const DetailProducts = () => {
  let [data, setData] = useState({});

  let params = useParams();

  let getDetails = async () => {
    let results = await axios({
      url: `http://localhost:8000/products/${params.id}`,
      method: "GET",
    });

    setData(results.data.data);
  };

  useEffect(() => {
    getDetails();
  }, []);

  console.log(data);

  return (
    <div>
      <p>Product Name is {data.name}</p>
      <p>Product Price is {data.price}</p>
      <p>Product In Stock is {data.isStock ? "Yes" : "No"}</p>
    </div>
  );
};

export default DetailProducts;
