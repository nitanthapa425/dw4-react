import React, { useState } from "react";
import axios from "axios";

const CreateProduct = () => {
  let [name, setName] = useState("");
  let [price, setPrice] = useState();
  let [isStock, setIsStock] = useState(false);
  let handleSubmit = async (e) => {
    e.preventDefault();
    let info = {
      name: name,
      price: price,
      isStock: isStock,
    };

    try {
      await axios({
        url: "http://localhost:8000/products",
        method: "POST",
        data: info,
      });
    } catch (error) {
      console.log(error.message);
    }
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label htmlFor="name">Name</label>
        <input
          type="text"
          id="name"
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
        ></input>
        <br></br>
        <br></br>
        <label htmlFor="price">Price:</label>
        <input
          type="number"
          id="price"
          value={price}
          onChange={(e) => {
            setPrice(e.target.value);
          }}
        ></input>
        <br></br>
        <br></br>
        <label htmlFor="isStock">isStock</label>
        <input
          type="checkbox"
          id="isStock"
          checked={isStock === true}
          onChange={(e) => {
            setIsStock(e.target.checked);
          }}
        ></input>
        <br></br>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default CreateProduct;
