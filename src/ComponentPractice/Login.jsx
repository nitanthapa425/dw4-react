import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const Login = () => {
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");

  let navigate = useNavigate();
  return (
    <div>
      <form
        onSubmit={async (e) => {
          e.preventDefault();
          let info = {
            email: email,
            password: password,
          };
          try {
            let result = await axios({
              url: "http://localhost:8000/registers/login",
              method: "POST",
              data: info,
            });

            localStorage.setItem("token", result.data.data);
            navigate("/products");
          } catch (error) {
            console.log(error.message);
          }
        }}
      >
        <label htmlFor="email">Email: </label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        ></input>
        <br></br>
        <label htmlFor="password">Password: </label>
        <input
          type="password"
          id="password"
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        ></input>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Login;
