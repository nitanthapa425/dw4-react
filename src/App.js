import React, { useState } from "react";
import LearnToHandleArray from "./ComponentPractice/LearnToHandleArray";
import LearnCleanupFunction from "./ComponentPractice/LearnUseEffect/LearnCleanupFunction";
import LearnCleanUpFunction2 from "./ComponentPractice/LearnUseEffect/LearnCleanUpFunction2";
import Form1 from "./ComponentPractice/LearnForm/Form1";
import Form2 from "./ComponentPractice/LearnForm/Form2";
import Form3 from "./ComponentPractice/LearnForm/Form3";
import LearnOrNullish from "./ComponentPractice/LearnOrNullish";
import Form4 from "./ComponentPractice/LearnForm/Form4";
import Form5 from "./ComponentPractice/LearnForm/Form5";
import Form6 from "./ComponentPractice/LearnForm/Form6";
import LearnUseRef from "./ComponentPractice/LearnUseRef/LearnUseRef";
import CreateProduct from "./ComponentPractice/LearnToHitApi/Product/CreateProduct";
import GetAllProduct from "./ComponentPractice/LearnToHitApi/Product/getAllProduct";
import LearnLocalStorage from "./ComponentPractice/LearnLocalStorage/LearnLocalStorage";

const App = () => {
  let [show, setShow] = useState(true);
  let [show1, setShow1] = useState(true);
  return (
    <div>
      {/* <LearnToHandleArray></LearnToHandleArray> */}

      {/* {show ? <LearnCleanupFunction></LearnCleanupFunction> : null}

      <button
        onClick={() => {
          setShow(true);
        }}
      >
        Show
      </button>
      <button
        onClick={() => {
          setShow(false);
        }}
      >
        Hide
      </button> */}

      {/* {show1 ? <LearnCleanUpFunction2></LearnCleanUpFunction2> : null}

      <button
        onClick={() => {
          setShow1(true);
        }}
      >
        Show
      </button>
      <button
        onClick={() => {
          setShow1(false);
        }}
      >
        Hide
      </button> */}

      {/* <Form1></Form1> */}
      {/* <Form2></Form2> */}
      {/* <Form3></Form3> */}
      {/* <Form4></Form4> */}
      {/* <Form5></Form5> */}
      {/* <Form6></Form6> */}
      {/* <LearnUseRef></LearnUseRef> */}
      {/* <LearnOrNullish></LearnOrNullish> */}

      {/* <CreateProduct></CreateProduct> */}
      {/* <getAllProduct></getAllProduct> */}
      {/* <GetAllProduct></GetAllProduct> */}

      <LearnLocalStorage></LearnLocalStorage>
    </div>
  );
};

export default App;
